# I'm Nick and I solve problems

Hey, welcome to my site/wiki/notebook/something. The idea for this site is to [build a second brain](https://www.buildingasecondbrain.com/) so my knowledge never gets lost, and perhaps, serves someone :)

I'm a software engineer based in [Gran Canaria](https://www.youtube.com/watch?v=4a46R3A0O34), where I happily live with my wife. 

I'm constantly searching for improving myself and the way that I do things. This led me to specialize myself in [statically typed functional programming](https://stackoverflow.com/questions/8147328/are-there-statically-typed-functional-languages). If you ask, [Haskell](https://www.haskell.org/) is my favorite programming language, although I feel comfortable switching between all of them. Since 2018, I'm in love with **Serverless**, and no, [this doesn't mean Functions as a Service, or no-servers](https://www.jeremydaly.com/stop-calling-everything-serverless/) :D

I currently work at [The Agile Monkeys](http://www.theagilemonkeys.com/), a company that makes me wake up with a smile on my face everyday. I resonate 100% with the company's philosophy, and it keeps me moving and growing professionally.

Personally, I consider myself a pretty chill person that likes to talk about mostly everything, feel free to talk to me, I'd love to have a chat with you :)

Wanna email me? I'm `nikitatchayka` using the google email service (hope this changes soon).
