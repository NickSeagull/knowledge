# Custom AWS Lambda Runtimes

## Container freezing

Apparently, lambda freezes the container when no further tasks are available.
The freezing period could be longer than the request timeout, which causes the
`invocation/next` request to fail with a timeout error.
[See this code in the official C++ runtime](https://github.com/awslabs/aws-lambda-cpp/blob/master/src/runtime.cpp#L203).
