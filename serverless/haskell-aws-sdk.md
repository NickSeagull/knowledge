# Haskell AWS SDK (amazonka)

## AWS Lambda timeout when contacting DynamoDB

Looks like if the `send` call returns an error, and you discard the result,
it will get crazy, and loop indefinitely, resulting in a Lambda timeout.

Instead of doing:

```haskell
-- Amazonka imported qualified as AWS

AWS.putItem streamName
|> AWS.piItem .~ dynamoItem
|> AWS.send
|> void
```

It's better to do this:

```haskell
let putItem = AWS.putItem streamName
              |> AWS.piItem .~ dynamoItem

response <- AWS.send putItem

let status = (response ^. AWS.pirsResponseStatus)
when (status /= 200) do
  print status
  error ( response ^. AWS.pirsAttributes
          |> encode
          |> decodeUtf8 )
```

What I suspect that is happening here is that Haskell's laziness is doing
something weird with the result, never returning from the mandatory call to
`AWS.runAWST`.
