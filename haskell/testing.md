# Testing techniques

## Mocking using Backpack

As Edward Kmett describes in [this Reddit comment](https://www.reddit.com/r/hascalator/comments/awtwny/simple_mocking_in_haskell/ehz27gv/?utm_source=share&utm_medium=web2x), it is possible to implement interfaces for modules without going through the usage of `mtl`-style type classes, [capabilities](https://github.com/tweag/capability), or [Freer Effects](https://github.com/lexi-lambda/freer-simple#readme) to allow mocking of effectful functions.

As Edward said:

> My preferred way to handle this is to design the API I want, and use it as a backpack module signature.

> Then I instantiate it for real once.

> And when I go to test it I instantiate it again, often against a different monad completely.

> This isn't perfect and runs into some problems with 's' parameters for things like `ST s`, but it has the benefit of zero runtime overhead, unlike the free monad approaches I often see in functional circles.

You'd build a module signature like

```haskell
signature API where

data M a

instance Functor M
instance Applicative M
instance Monad M

...

whatever :: M Int
```

When you write the rest of the package use this module rather than a concrete instantiation.

```haskell
module CoolStuff where

import API

foo :: M Bool
foo = even <$> whatever
```

in cabal this looks something like

```cabal
library core
  hs-source-dirs: src/core
  signatures: API
  exposed-modules: CoolStuff
```

then build packages that export modules that include a definition for some type synonym M that has those instances and a whatever method.

```haskell
-- API/IO.hs
module API.IO where
type M = IO
whatever :: IO Int
whatever = ...

-- API/Fake.hs

module API.Fake where
type M = (->) Int
whatever :: M Int
whatever = id
```

with library stanzas like

```cabal
library io-core
  hs-source-dirs: src/io
  module: API.IO

library fake-core
  hs-source-dirs: src/fake
  module: API.Fake
```

Then you can build an instantiation of the backpack signature you gave by building little libraries that use these.

```cabal
library actual-production-core
  build-depends: base, core, io-core
  mixins: core (CoolStuff as Production.CoolStuff) requires (API as API.IO)
  reexported-moduiles: Production.CoolStuff
```

Then you can use actual-production-core when you ship your application and a similar fake-test-core when you are building your test suite.

Will it actually be a reader-based M in the test suite? Probably not, but you can build any monad you want for testing this way without paying any performance tax in production when linking against the other package. All the "meat" of the package lives in "core", its only when you start caring about if you are in production or test that you link against the later packages.

**In my opinion,** this is super nice when writing performance-critical code, but in general development, I prefer to see explicit events like with the capability approach, or the effects approach.

For example, you could write something like this using this approach:

```haskell
-- My function definition, 'FileSystem' is a "faked"
-- type to allow specific file system IO calls.
generateConfig :: Text -> FileSystem ()
```

It is as explicit as the capabilities, or effects, approaches:

```haskell
-- Capabilities approach
generateConfig' :: (HasFileSystem io) => Text -> io ()

-- Freer effects approach
generateConfig'' :: (Member FileSystem effs) => Text -> Eff effs ()
```

But what happens if we want to add logging to this? In the two latter approaches, it is straightforward.

```haskell
-- Capabilities approach
generateConfig' :: (HasFileSystem io, HasLogging io) => Text -> io ()

-- Freer effects approach
generateConfig'' :: (Members '[FileSystem, Logging] effs) => Text -> Eff effs ()
```

While I have no idea how to do the same with Edward's approach.

