# Libraries

This page serves as a board to research libraries, and conclude if they fit in my stack of libraries or not.

## Current stack

TODO: Write something here

## Currently trying

TODO: Write something here

## To be researched

### [Typed Process](https://haskell.fpcomplete.com/library/typed-process)

From the website:

This library provides the ability to launch and interact with external processes. It wraps around the process library, and intends to improve upon it by:

* Using type variables to represent the standard streams, making them easier to manipulate
* Use proper concurrency (e.g., the async library) in place of the weird lazy I/O tricks for such things as consuming output streams
* Allow for more complex concurrency by providing STM-based functions
* Using binary I/O correctly
* Providing a more composable API, designed to be easy to use for both simple and complex use cases

At first glance, it looks awesome, not sure about the drawbacks though.
