# Good Practices

Some notes of posts I've been seeing on the internet.

## Avoid the `M_` functions

As `/u/ElvishJerricco` stated in [this comment](https://www.reddit.com/r/haskell/comments/5990fo/whats_the_point_of_foldable_for_simple_types_like/d980b67?utm_source=share&utm_medium=web2x), there are better functions that are alternatives to the typical ones that end in `M_` (better means more abstract, so you can use them not only with types that instantiate the `Monad` class):

| **Old**        | **New**         |
|----------------|-----------------|
| `liftM f a`    | `fmap f a`      |
| `liftM2 f a b` | `f <$> a <*> b` |
| `ap f a`       | `f <*> a`       |
| `forM a f`     | `for a f`       |
| `forM_ a f`    | `for_ a f`      |
| `sequence a`   | `sequenceA a`   |
