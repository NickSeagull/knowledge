# Everyday Carry

I don't carry many things daily. If I'm going to the store or to the cinema, I just take with me:

* Messenger bag
* Degooglified Xiaomi MiA1
* Wallet
* Home keys
* Car keys

But when I go to work, I have to take a little bit more:

* A backpack instead of the bag
* Thinkpad X250
* Nexstand laptop stand
* USB Hub
* Kinesis Advantage2
* Logitech M570
* Some kind of headset for meetings
* A notebook with my fountain pen
* Or an iPad 2018 instead of the notebook.
* Phone charger (USB Type C)
* Laptop charger
